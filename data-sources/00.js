
(function handlePage() {
    const providers = [...document.querySelectorAll('.provider-row')]

    const extxt = (e,s='*',cb)=>{
        const f = e.querySelector && e.querySelector(s) || e
        if (cb)
            return cb(f)
        const ret = f && f.textContent !== undefined && f.textContent.trim()
        if (!ret && ret!=='') return f;
        return ret;
    }

    const ret = providers.map((e,i,f)=>{
        return {
            name: extxt(e, '.company-name'),

            website: extxt(e, '.website-link a', e=>e.href),
            profile: e.querySelector('.website-link + li') && e.querySelector('.website-link + li').querySelector('a').href,

            reviewsCount: extxt(e, '.reviews-count').match(/\d+/)[0],
            size: e.querySelector('.icon-person') && extxt(e.querySelector('.icon-person').nextSibling),

            tagline: extxt(e, '.tagline'),
            projectSize: e.querySelector('.icon-paid') && extxt(e.querySelector('.icon-paid').nextSibling),
            serviceFocus: [...e.querySelectorAll('.chartAreaContainer .grid')].map(e=>e.getAttribute('data-content')),
            locality: extxt(e, '.locality'),
            region: extxt(e, '.region'),
        }
    }
    )

    console.info('ret', ret)

//     persistToDb(ret, () => {})
    persistToDb(ret, moveForward)

    function moveForward() {
        const link = [...document.querySelectorAll('a')].find(e=>e.textContent.startsWith('next'))
        console.info('fetching', link)
        if (link) {
            fetch(link.href).then(r=>r.text()).then(r=>{
                var parser = new DOMParser();
                var doc = parser.parseFromString(r, "text/html");
                const toReplace = '#providers'
                var el = doc.querySelector(toReplace);
                const d = document.querySelector(toReplace)
                d.innerHTML = el.innerHTML
                setTimeout(()=>handlePage(), 100)
            }
            )
        }
    }

    function persistToDb(data, nextCb) {
        const dbName = "ClutchExport";

        var request = indexedDB.open(dbName, 2);

        request.onerror = function(event) {}
        request.onupgradeneeded = function(event) {
            var db = event.target.result;

            var objectStore = db.createObjectStore("firms", {
                keyPath: "profile"
            });

            objectStore.transaction.oncomplete = function(event) {
                var customerObjectStore = db.transaction("firms", "readwrite").objectStore("firms");
            }
        }

        request.onsuccess = function(event) {
            var db = event.target.result;
            data.forEach(function(customer) {

                var customerObjectStore = db.transaction(["firms"], "readwrite").objectStore("firms");
                try {
                    if(customer.profile === null) {console.info('skipping', customer.profile, customer); return}; //SKIP INVALID ROWS
                const r = customerObjectStore.add(customer)
                r.onsuccess = e=>console.info(customer.name || customer, e);
                r.onerror = e=>
                {
                    const msg = e.target.error.message;
                    console.error(customer.name || customer, e && e.target && e.target.error || e);
                    if(msg != "Key already exists in the object store.") debugger;
                }
                console.warn(r)

                } catch (e) {
                    const error = e, cust = customer;
                  debugger;
                }
            });
        }

        nextCb()
    }

}
)()
